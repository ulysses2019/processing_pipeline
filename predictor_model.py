import pickle

f = open("logreg_model.sav", "rb")
PREDICTOR_MODEL = pickle.load(f)

f2 = open("scaler.sav", "rb")
SCALER_MODEL = pickle.load(f2)


def scale_features(features):
    scaled_features = SCALER_MODEL.transform(features)
    return scaled_features


def get_prediction(list_features):
    """"
    list_features: a list of floats with the features extracted from the text.
    with the following format: [[1.0,3.9,....]] size of list_features[0] is 768
    """
    prediction_probabilities = list(PREDICTOR_MODEL.predict_proba(list_features)[0])
    prediction_dict = {class_: "{0:.2f}".format(probability * 100) for class_, probability \
                       in zip(PREDICTOR_MODEL.classes_, prediction_probabilities)}

    return prediction_dict
