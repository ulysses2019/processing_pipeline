from predictor_model import get_prediction, scale_features
from flask import Flask
from celery import Celery
from conf import REDIS_ADDRESS
from conf import REDIS_PORT
import numpy as np


flask_app = Flask(__name__)
flask_app.config.update(
    CELERY_BROKER_URL=f'redis://{REDIS_ADDRESS}:{REDIS_PORT}',
    CELERY_RESULT_BACKEND=f'redis://{REDIS_ADDRESS}:{REDIS_PORT}'
)


def make_celery(app):
    celery = Celery(
        app.import_name,
        backend=app.config['CELERY_RESULT_BACKEND'],
        broker=app.config['CELERY_BROKER_URL']
    )
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery


celery_app = make_celery(flask_app)


def create_json_object(prediction):
    # input (prediction)
    # {'A1': '0.02', 'A2': '0.00', 'B1': '0.22', 'B2': '31.95', 'C1': '48.56', 'C2': '19.24'}

    # output
    # {'first_class': 'C2',
    #  'first_class_percentage': '48.81%',
    # 'second_class': 'C1',
    # 'second_class_percentage': '31.99%'
    # }

    first_class = None
    second_class = None
    first_class_percentage = 0
    second_class_percentage = 0
    print(prediction.items())
    for key, value in prediction.items():
        if float(value) > first_class_percentage:
            first_class = key
            first_class_percentage = float(value)

    for key, value in prediction.items():
        if first_class_percentage >= float(value) > second_class_percentage and first_class != key:
            second_class = key
            second_class_percentage = float(value)

    output = {
        'first_class': first_class,
        'first_class_percentage': str(first_class_percentage) + "%",
        'second_class': second_class,
        'second_class_percentage': str(second_class_percentage) + "%"
    }

    return output


@celery_app.task()
def generate_prediction(text):

    features = execute_preprocessing_pipeline(text)
    print('****************** Going to scale features *****************************************')
    features_scaled = scale_features(features)
    prediction = get_prediction(features_scaled)
    response_json_object = create_json_object(prediction)
    return response_json_object



def execute_preprocessing_pipeline(text: str) -> list:
    """"Function that receives a text (str) and returns a list of features extracted from
    the text (float type)
    input: str
    output: list of float
    """
    # mocking an output
    # HERE YOU PLUG  YOUR CODE THAT GENERATES FEATURES.
    return [[np.random.random_sample() * 1000 for i in range(0, 768)]]