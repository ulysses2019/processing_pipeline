FROM ubuntu:18.04
RUN apt-get update -y \
    && apt-get install -y \
        git \
        build-essential \
        python-pip \
        python-dev \
        build-essential \
        python3-pip \
        python3-dev \
        default-jdk \
        libenchant1c2a \
    && rm -rf /var/lib/apt/lists/*

ADD ./ /ulysses_preprocessing
WORKDIR /ulysses_preprocessing
RUN pip3 install -r requirementspy3.txt
RUN chmod +x run_worker.sh
RUN apt-get update -y \
    && apt-get install -y \
    locales && locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
CMD ["./run_worker.sh"]


